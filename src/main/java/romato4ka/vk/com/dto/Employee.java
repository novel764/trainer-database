package romato4ka.vk.com.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Employee {
    private int id;
    private String name;
    private String lastname;
    private String email;
    private String language;
    private int experience;
}