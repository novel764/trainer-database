package romato4ka.vk.com.dao.impl;

import romato4ka.vk.com.dao.DAOEmployee;
import romato4ka.vk.com.dto.Employee;

import java.util.ArrayList;
import java.util.List;

public class DAOEmployeeImplTwo implements DAOEmployee {
    private static List<Employee> employees = new ArrayList<>();

    static {
        employees.add(Employee.builder()
                .name("1")
                .lastname("1")
                .experience(1)
                .language("1")
                .email("1")
                .id(1)
                .build());
        employees.add(Employee.builder()
                .name("2")
                .lastname("2")
                .experience(2)
                .language("2")
                .email("2")
                .id(2)
                .build());
        employees.add(Employee.builder()
                .name("3")
                .lastname("3")
                .experience(3)
                .language("3")
                .email("3")
                .id(3)
                .build());
        employees.add(Employee.builder()
                .name("4")
                .lastname("4")
                .experience(4)
                .language("4")
                .email("4")
                .id(4)
                .build());
        employees.add(Employee.builder()
                .name("5")
                .lastname("5")
                .experience(5)
                .language("5")
                .email("5")
                .id(5)
                .build());
    }

    @Override
    public List<Employee> findAllEmployees() {
        return employees;
    }

    @Override
    public Employee findEmployeeById(int id) {
        return employees.stream()
                .filter(val -> val.getId() == id)
                .findFirst()
                .orElseThrow();
    }

    @Override
    public void insertEmployee(Employee employee) {
        employees.add(employee);
    }

    @Override
    public void updateEmployee(Employee employee, int id) {
        employees.add(employees.indexOf(employees.stream()
                .filter(val -> val.getId() == id)
                .findFirst()
                .orElseThrow()), employee);
    }

    @Override
    public void deleteEmployee(int id) {
        employees.remove(id);
    }
}
