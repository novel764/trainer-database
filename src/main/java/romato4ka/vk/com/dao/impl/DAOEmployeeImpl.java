package romato4ka.vk.com.dao.impl;

import romato4ka.vk.com.dao.DAOEmployee;
import romato4ka.vk.com.dto.Employee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static romato4ka.vk.com.configuration.ConfigurationDatabase.getConnection;

public class DAOEmployeeImpl implements DAOEmployee {

    private static final String FIELD_EMPLOYEE_ID = "employee_id";
    private static final String FIELD_EMPLOYEE_NAME = "employee_name";
    private static final String FIELD_EMPLOYEE_LASTNAME = "employee_lastname";
    private static final String FIELD_EMAIL = "email";
    private static final String FIELD_PROGRAMMING_LANGUAGE = "programming_language";
    private static final String FIELD_EXPERIENCE = "experience";

    private static final String FIND_ALL_EMPLOYEE = """
            select employee_id, employee_name, employee_lastname, email, programming_language, experience
            from employee""";

    private static final String FIND_EMPLOYEE_BY_ID = """
            select employee_id, employee_name, employee_lastname, email, programming_language, experience
            from employee
            where employee_id = ?""";

    private static final String INSERT_EMPLOYEE = """
            INSERT INTO employee (employee_name, employee_lastname, email, programming_language, experience)
            VALUES (?, ?, ?, ?, ?)""";

    private static final String UPDATE_EMPLOYEE = """
            UPDATE employee
            SET employee_name        = ?,
                employee_lastname    = ?,
                email                = ?,
                programming_language = ?,
                experience           = ?
            WHERE employee_id = ?""";
    private static final String DELETE_EMPLOYEE = """
            DELETE
            FROM employee
            WHERE employee_id = ?""";


    @Override
    public List<Employee> findAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_EMPLOYEE)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                employees.add(Employee.builder()
                        .id(resultSet.getInt(FIELD_EMPLOYEE_ID))
                        .name(resultSet.getString(FIELD_EMPLOYEE_NAME))
                        .lastname(resultSet.getString(FIELD_EMPLOYEE_LASTNAME))
                        .email(resultSet.getString(FIELD_EMAIL))
                        .language(resultSet.getString(FIELD_PROGRAMMING_LANGUAGE))
                        .experience(resultSet.getInt(FIELD_EXPERIENCE))
                        .build());
            }
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
        return employees;
    }

    @Override
    public Employee findEmployeeById(int id) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_EMPLOYEE_BY_ID)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Employee.builder()
                        .id(resultSet.getInt(FIELD_EMPLOYEE_ID))
                        .name(resultSet.getString(FIELD_EMPLOYEE_NAME))
                        .lastname(resultSet.getString(FIELD_EMPLOYEE_LASTNAME))
                        .email(resultSet.getString(FIELD_EMAIL))
                        .language(resultSet.getString(FIELD_PROGRAMMING_LANGUAGE))
                        .experience(resultSet.getInt(FIELD_EXPERIENCE))
                        .build();
            }
            return null;
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void insertEmployee(Employee employee) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_EMPLOYEE)) {
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setString(2, employee.getLastname());
            preparedStatement.setString(3, employee.getEmail());
            preparedStatement.setString(4, employee.getLanguage());
            preparedStatement.setInt(5, employee.getExperience());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void updateEmployee(Employee employee, int id) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_EMPLOYEE)) {
            preparedStatement.setString(1, employee.getName());
            preparedStatement.setString(2, employee.getLastname());
            preparedStatement.setString(3, employee.getEmail());
            preparedStatement.setString(4, employee.getLanguage());
            preparedStatement.setInt(5, employee.getExperience());
            preparedStatement.setInt(6, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void deleteEmployee(int id) {
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_EMPLOYEE)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
