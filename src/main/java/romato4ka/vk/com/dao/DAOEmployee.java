package romato4ka.vk.com.dao;

import romato4ka.vk.com.dto.Employee;

import java.sql.SQLException;
import java.util.List;

public interface DAOEmployee {
    List<Employee> findAllEmployees();
    Employee findEmployeeById(int id);
    void insertEmployee(Employee employee);
    void updateEmployee(Employee employee, int id);
    void deleteEmployee(int id);
}
