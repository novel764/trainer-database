package romato4ka.vk.com.service;

import romato4ka.vk.com.dto.Employee;

import java.sql.SQLException;
import java.util.List;

public interface EmployeeService {
    List<Employee> findAllEmployees();
    Employee findEmployeeById(int id);
    void insertEmployee(Employee employee);
    void updateEmployee(Employee employee);
    void deleteEmployee(int id);
}
