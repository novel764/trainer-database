package romato4ka.vk.com.service.impl;

import romato4ka.vk.com.dao.DAOEmployee;
import romato4ka.vk.com.dao.impl.DAOEmployeeImpl;
import romato4ka.vk.com.dao.impl.DAOEmployeeImplTwo;
import romato4ka.vk.com.dto.Employee;
import romato4ka.vk.com.service.EmployeeService;

import java.util.List;

public class EmployeeServiceImpl implements EmployeeService {
    private final DAOEmployee daoEmployee = new DAOEmployeeImpl();

    @Override
    public List<Employee> findAllEmployees() {
        return daoEmployee.findAllEmployees();
    }

    @Override
    public Employee findEmployeeById(int id) {
        return daoEmployee.findEmployeeById(id);
    }

    @Override
    public void insertEmployee(Employee employee) {
        daoEmployee.insertEmployee(employee);
    }

    @Override
    public void updateEmployee(Employee employee) {
        if (employee.getLastname().equals("Borgyakov")) {
            throw new RuntimeException("Нельзя менять его фамилию");
        }
        daoEmployee.insertEmployee(employee);
    }

    @Override
    public void deleteEmployee(int id) {
        daoEmployee.deleteEmployee(id);
    }
}
