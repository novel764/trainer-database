package romato4ka.vk.com;

import romato4ka.vk.com.dao.DAOEmployee;
import romato4ka.vk.com.dao.impl.DAOEmployeeImpl;
import romato4ka.vk.com.dto.Employee;
import romato4ka.vk.com.service.EmployeeService;
import romato4ka.vk.com.service.impl.EmployeeServiceImpl;

import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EmployeeService daoEmployee = new EmployeeServiceImpl();
        Employee build = Employee.builder()
                .name("123")
                .lastname("123")
                .experience(12)
                .email("123@gmail.com")
                .language("123")
                .build();
        daoEmployee.insertEmployee(build);
        daoEmployee.findAllEmployees().forEach(System.out::println);
    }
}
