package romato4ka.vk.com.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConfigurationDatabase {

    private static final HikariConfig config = new HikariConfig();
    private static final HikariDataSource HIKARI_DATA_SOURCE;

    static {
        Properties props = new Properties();
        try (FileInputStream fis = new FileInputStream("src/main/resources/db.properties")) {
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }

        config.setJdbcUrl(props.getProperty("POSTGRES_DB_URL"));
        config.setUsername(props.getProperty("POSTGRES_DB_USERNAME"));
        config.setPassword(props.getProperty("POSTGRES_DB_PASSWORD"));
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        HIKARI_DATA_SOURCE = new HikariDataSource(config);
    }

    private ConfigurationDatabase() {
    }

    public static Connection getConnection() throws SQLException {
        return HIKARI_DATA_SOURCE.getConnection();
    }

}
